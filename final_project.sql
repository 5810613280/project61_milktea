-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 19, 2018 at 03:43 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--
/*
CREATE TABLE `comment` (
  `comment_id` int(11) UNSIGNED NOT NULL,
  `webboard_id` int(11) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `webboard_id`, `comment`, `timestamp`, `user_id`) VALUES
(28, 8, 'Love that too!! xoxo', '2017-12-30 11:55:10', 5),
(29, 7, 'xoxoxo!', '2017-12-30 12:05:14', 5),
(30, 6, 'ชอบเหมือนกันค่ะ (:', '2017-12-30 12:05:30', 5),
(31, 9, 'เคยเจอคนเอาไปผสมกับเบสหรือไม่พวกกันแดดอ่ะจ้ะ', '2017-12-30 14:52:13', 4),
(32, 9, 'ซื้อแบรนด์ไหนมาคะถ้าเปลี่ยนได้ก็เปลี่ยนค่า ถ้าไม่ได้ก็เอามาทำเป็นเฉดดิ้งแบบครีมก็ได้น้า  หรือจะไปซื้อสีอ่อนมาผสมก็ได้ค่ะ แต่สำหรับเรามันคงยุ่งยากอาจจะขายต่อค่ะ', '2017-12-30 14:52:58', 4),
(33, 9, 'เราผิวเข้มNC40  พวกเบสเกาหลีนี่จะทำให้หน้าเราขาวมากกกกกกกกกเหมือนหน้าจูออน ขาวแบบเลเวลเกินเทาไปอีก แต่พอเอามาทาทับด้วยรองพื้นที่เข้มกว่าผิว\r\nมันจะออกมาพอดีๆมีผ่องๆนิดนุงแถมรองพื้นเนื้อไม่เพี้ยน', '2017-12-30 14:54:13', 4),
(34, 9, 'เราใช้EtudeFixandFixสีชมพู แต่เห็นมีคนบอกว่าสีม่วงก็ใช้ได้เหมือนกัน ซื้อตามshoppeeราคา3ร้อยนิดๆก็โอเคอยู่ดีกว่าใช้รองพื้นไม่ได้ไปเลย  วิธีนี้เวิร์ค\r\n', '2017-12-30 14:54:32', 4),
(35, 9, 'เราใช้เบสเขียวผสมค่ะ', '2017-12-30 14:54:55', 4);

-- --------------------------------------------------------
*/
--
-- Table structure for table `flight_board`
--
/*
CREATE TABLE `flight_board` (
  `flight_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(280) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `flight_board`
--

INSERT INTO `flight_board` (`flight_id`, `user_id`, `title`, `post`, `timestamp`) VALUES
(7, 4, 'Bangkok - Ubon Ratchathani', 'Hi guys! Sharing some of my beauty favorites on the blog today :) For the longest time I wasn’t really into makeup and didn’t branch out and try a ton of new products but for the past couple months i’ve been really into finding new things to try! I wanted to do a post on some of my new favorites and let you guys know what I think about them! Also comment below any of your summer favorites or any blog post ideas!\r\n\r\n1. Tarte Brazilliance Plus Self Tanner – This stuff is AMAZING! I have really fair skin so I am always trying to figure out a good self tanner and I really really like this stuff. It’s a gel and it comes with a mit that is sooooo soft!! I have used the St. Tropez bronzing mousse before and it’s WAY too dark for me so I like this a lot more. I always have streaks and lines all over from self tanning because I am just really bad at it apparently haha but I feel like with this I am improving! I visited the Tarte offices a couple weeks ago and they gave me this to try and they also gave me this spray tanner which I haven’t tried yet but all the girls were saying it’s amazing. Also they have these awesome face tanning wipes which is SO COOL because i don’t like to put regular self tanner on my face so these are perfect!!! Definitely some of my summer favorites.\r\n\r\n2. Tarte Rainforest of the Sea Foundation – My new favorite foundation! I also got this at the Tarte offices as I was finishing up my It Cosmetics CC Cream (which I also like) but I was ready to try something new and I love it so much! It’s hydrating and gives a nice glow and good coverage. My skin is kind of oily but also can look really dry especially with powder so I like that this gives a really fresh look!\r\n\r\n3. Bare Minerals Sheer Sun Serum Bronzer – I like to mix this liquid bronzer with my foundation since it’s a little too light for me when I self tan. It’s sheer so it’s perfect to mix right in!\r\n\r\n4. Bottom Lash Mascara – Why am I just discovering this?!? I have eyelash extensions so I only wear mascara on my bottom eyelashes and normally I just use my Covergirl Lashblast but since the brush is so big and my bottom eyelashes are kind of long I always end up with mascara all over my under eye area and I look like I have black eyes… I like lower lash mascaras because the brush is so small so what I have been doing lately is only coating the top half of my lower lashes with mascara so it still defines them but there isn’t any actual mascara on the ends of my lashes so no mascara gets under my eyes!\r\n\r\n5. Nars Albatross Highlighter – I love this shade! It’s very brightening and has a cool look but also some golden shimmer… this has been my go-to for a few weeks now!\r\n\r\n6. Tarte Lip Paint – Normally I don’t really wear lip colors but lately I have been loving this Namaste shade! This is a made lip paint, but I also like the Goals shade in the glossy lip paint version!\r\n\r\n7. Benefit Brow Gel – This is the only brow gel I have ever tried that actually holds my eyebrows in place. I have crazy brows so this is an essential for me!\r\n\r\n8. Pixi Glow Mist – I love using this to set my makeup! It gives a dewy, glow-y look that’s perfect for summer.\r\n\r\n9. Pixi Eye Brightener – I use this to brighten my under eye area and I love it! I feel like under my eyes is always really dark so I use this to help solve that problem!\r\n\r\n10. Tarte Amazonian 12 Hour Blush – Another great Tarte product! I have a few different shades of this blush and they are all great! Tarte is becoming one of my new favorite makeup brands!', '2018-05-05 05:28:19'),
(8, 4, 'Bangkok - Udon Thani', '16.00-17.30', '2018-05-05 05:28:35'),
(9, 4, 'Bangkok - Khon Kaen', '12.00-13.30', '2018-05-05 05:28:50'),
(10, 4, 'Bangkok - Chiang Rai', '10.30-12.00', '2018-05-05 05:29:01'),
(11, 6, 'Bangkok - Chiang Mai', '10.00-11.30\r\n13.00-14.30\r\n15.00-16.30', '2018-05-05 05:29:07'),
(13, 6, 'Bangkok - Trang', '10.30-12.30', '2018-05-05 05:31:54'),
(14, 6, 'Bangkok - Phuket', '13.30-16.30', '2018-05-05 05:04:15'),
(15, 6, 'Bangkok - Hatyai', '12.00-13.00', '2018-05-05 05:03:41'),
(16, 6, 'Bangkok - Surat Thani', '13.00-14.30', '2018-05-05 05:29:35'),
(17, 6, 'Bangkok - Krabi', 'กกก', '2018-05-05 05:02:35'),
(18, 6, 'Trang - Bangkok', 'dddd', '2018-05-05 05:43:56'),
(19, 6, 'Surat Thani - Bangkok ', 'ssss', '2018-05-05 05:44:24'),
(20, 6, ' Chiang Mai - Bangkok ', 'hhh', '2018-05-05 05:45:02'),
(21, 6, 'Chiang Rai - Bangkok', 'x', '2018-05-05 05:45:19'),
(22, 6, 'Khon Kaen - Bangkok ', 'x', '2018-05-05 05:45:46'),
(23, 6, 'Udon Thani - Bangkok ', 'x', '2018-05-05 05:46:07'),
(24, 6, 'Ubon Ratchathani - Bangkok ', 'x', '2018-05-05 05:46:54'),
(25, 6, 'Phuket - Bangkok ', 'x', '2018-05-05 05:47:20'),
(26, 6, ' Hatyai - Bangkok', 'x', '2018-05-05 05:48:05'),
(27, 6, 'Krabi  - Bangkok', 'x', '2018-05-05 05:48:25');
*/
-- --------------------------------------------------------

--
-- Table structure for table `new_reserve`
--
/*
CREATE TABLE `new_reserve` (
  `reservation_id` int(11) NOT NULL,
  `reserve_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `people` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `new_reserve`
--

INSERT INTO `new_reserve` (`reservation_id`, `reserve_id`, `timestamp`, `user_id`, `promotion_id`, `people`) VALUES
(1, 102, '2018-05-09 17:22:29', 9, 1, 1),
(7, 105, '2018-05-09 18:07:21', 9, 1, 1),
(8, 111, '2018-05-09 18:10:37', 9, 1, 1),
(9, 111, '2018-05-09 18:11:01', 9, 1, 1),
(10, 111, '2018-05-09 18:11:21', 9, 1, 1),
(11, 111, '2018-05-09 18:12:29', 9, 1, 1),
(12, 117, '2018-05-09 18:22:14', 9, 8, 4),
(13, 108, '2018-05-09 18:23:41', 9, 8, 4),
(14, 108, '2018-05-09 18:24:07', 9, 8, 4),
(15, 108, '2018-05-09 18:24:56', 9, 8, 4),
(16, 102, '2018-05-10 12:35:13', 11, 1, 1),
(17, 102, '2018-05-10 12:36:53', 11, 1, 1),
(18, 102, '2018-05-10 14:59:11', 11, 1, 1),
(19, 103, '2018-05-10 15:00:20', 11, 1, 1),
(20, 118, '2018-05-10 15:01:41', 11, 1, 1),
(21, 109, '2018-05-10 15:03:15', 11, 1, 1),
(22, 112, '2018-05-10 15:03:50', 11, 1, 1),
(23, 105, '2018-05-10 16:02:54', 11, 1, 1),
(24, 107, '2018-05-10 17:05:56', 11, 4, 1);
*/
-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `post_id` int(11) NOT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`post_id`, `post_title`, `img_name`, `post_content`, `post_time`) VALUES
(1, 'LUXURY BRANDS THAT ARE CHANGING THE VEGAN SKINCARE GAME', 'Vegan-Skincare.jpg', 'When I was in college over 12 years ago, veganism looked like this: waif-thin, black-rimmed glasses, hardcore band T-shirts, tattoo sleeves and sometimes a very militant attitude to put the image over the edge. As a vegetarian at the time, I certainly had respect for the vegans I knew for having the conviction to eschew any and all animal products. But it definitely felt radical, even to someone as progressive as myself. In those days, the lifestyle had everything to do with politics and, for the most part, very little to do with clean eating and skin care.\r\n\r\nFast forward to 2017 and wow—veganism looks more like Jessica Chastain and Liam Hemsworth. Don’t get me wrong, I’m not exactly giving celebs credit for putting that cruelty-free life on the map years after predecessors who’ve been woke were already entrenched in it. But I’d be lying if I said that the new social acceptance of veganism in the last couple of years didn’t influence me to take the leap for myself.\r\n\r\nBefore I even gave up dairy and eggs, I was already vigilant about avoiding beauty products that are tested on animals. To me, there’s just no sensible reason for the archaic practice to continue. Also, if formulas are loaded with synthetic additives that need to be tested, I personally feel it’s risky to put those chemicals on my skin—and on my eyes! When I went vegan last year I felt I had to streamline my beauty arsenal even more. (Full disclosure: Don’t be mad, but there’s at least one staple I haven’t let go of yet that has beeswax.)\r\n\r\nAre “natural” and vegan products less effective? Well, many beauty marketers and some aestheticians and dermatologists might say so if conventional skincare is what they’re selling. I simply don’t buy that. Plant-based products harness the power of organic ingredients that are rich in antioxidants and phytonutrients (natural chemicals that protect plants from environmental stressors). Veggies and fruits are pretty indisputably the healthiest foods to nourish from within, so it makes sense that they do good on the outside, too.\r\n\r\nLook, I know vegan skincare isn’t for everyone, but the trend toward organic living and ingredient-conscious skincare is undeniable. Considering so many amazing brands could be vegan if it weren’t for one small piece of the ingredient puzzle (e.g. honey, lanolin and carmine), disrupting the narrative around this once niche market is important because those who want high-performance plant-based beauty would love more options. Thankfully, here are six luxury skincare brands that are 100-percent vegan.', '2017-12-30 04:51:02');

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `promotion_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name_tag` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_tag` int(11) NOT NULL,
  `file_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`promotion_id`, `user_id`, `name_tag`, `price_tag`, `file_name`, `package`, `timestamp`) VALUES
(1, 4, 'Surat', 2800, 'surath.jpg', 'Flight-Hotel', '2018-05-05 14:15:46'),
(2, 4, 'Phuket', 4000, 'phuket.jpg', 'Flight', '2018-05-04 18:24:47'),
(3, 4, 'Samui', 8000, 'smui.jpg', 'Flight-Tour', '2018-05-05 07:45:09'),
(4, 4, 'Krabi', 6000, 'krabee.png\r\n', 'Flight-Hotel', '2018-05-05 07:45:25'),
(5, 4, 'Hatyai', 6000, 'hatyai.jpg', 'Flight', '2018-05-04 18:29:36'),
(6, 4, 'Ubon', 1200, 'ubon.jpg', 'Flight', '2018-05-04 18:30:25'),
(7, 4, 'Udon Thani', 3000, 'udon.jpg', 'Flight-Hotel-Tour', '2018-05-05 07:45:53'),
(8, 4, 'Khon Kaen', 4400, 'khonkean.jpg', 'Flight-Hotel', '2018-05-04 18:51:43'),
(9, 4, 'Chiang Mai', 6500, 'chingmai.jpg', 'Flight-Hotel-Tour', '2018-05-04 18:53:12'),
(10, 4, 'Chiang Rai', 5000, 'chingrai.jpg', 'Flight-Hotel', '2018-05-04 18:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `reserve`
--

CREATE TABLE `reserve` (
  `reserve_id` int(11) UNSIGNED NOT NULL,
  `flight_id` int(11) UNSIGNED NOT NULL,
  `reserve` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reserve`
--

INSERT INTO `reserve` (`reserve_id`, `flight_id`, `reserve`, `timestamp`, `user_id`) VALUES
(57, 11, 'Flight DD8302    ->   07:00 to 08:10', '2018-05-05 05:18:51', 6),
(58, 11, 'Flight DD8306     ->   09:05 to 10:15', '2018-05-05 05:19:14', 6),
(59, 11, 'Flight DD8312    ->  12:40 to 13:50', '2018-05-05 05:19:45', 6),
(60, 11, 'Flight DD8316 -> 14:20 to 15:30', '2018-05-05 05:20:11', 6),
(61, 11, 'Flight DD8318  -> 16:30 to 17:40', '2018-05-05 05:20:27', 6),
(62, 11, 'Flight DD8324  ->  19:00 to 20:15', '2018-05-05 05:20:42', 6),
(63, 11, 'Flight DD8326 -> 20:15 to 21:25', '2018-05-05 05:21:05', 6),
(64, 10, 'Flight DD8714  ->  07:20 to 08:30', '2018-05-05 05:24:04', 6),
(65, 10, 'Flight DD8718  ->  13:10 to 14:25', '2018-05-05 05:24:20', 6),
(66, 10, 'Flight DD8722  ->  16:40 to 17:55', '2018-05-05 05:24:34', 6),
(67, 9, 'Flight DD9806 -> 06:30 to 07:20', '2018-05-05 05:25:12', 6),
(68, 9, 'Flight DD9814  ->  11:05 to 11:50', '2018-05-05 05:25:26', 6),
(70, 9, 'Flight DD9820  ->  18:40 to 19:35', '2018-05-05 05:26:09', 6),
(71, 8, 'Flight DD9200  ->  06:00 to 07:05', '2018-05-05 05:26:56', 6),
(72, 8, 'Flight DD9202  -> 10:00 to 11:05', '2018-05-05 05:27:12', 6),
(73, 8, 'Flight DD9210  ->  12:20 to 13:25', '2018-05-05 05:27:30', 6),
(74, 8, 'Flight DD9214  ->  15:25 to 16:35', '2018-05-05 05:27:43', 6),
(75, 8, 'Flight DD9216  -> 17:00 to 18:05', '2018-05-05 05:28:00', 6),
(76, 16, 'Flight DD7208 -> 06:10 to 07:20', '2018-05-05 05:30:14', 6),
(77, 16, 'Flight DD7210 -> 09:20 to 10:30', '2018-05-05 05:30:31', 6),
(78, 16, 'Flight DD7212  -> 12:40 to 13:50', '2018-05-05 05:30:48', 6),
(79, 16, 'Flight DD7216 -> 16:25 to 17:35', '2018-05-05 05:31:01', 6),
(80, 13, 'Flight DD7400 -> 07:30 to 08:50', '2018-05-05 05:32:09', 6),
(81, 13, 'Flight DD7406 -> 11:10 to 12:25', '2018-05-05 05:32:22', 6),
(82, 13, 'Flight DD7410 -> 16:00 to 17:20', '2018-05-05 05:32:36', 6),
(83, 7, 'Flight DD9312 -> 07:25 to 08:30', '2018-05-05 05:33:19', 6),
(84, 7, 'Flight DD9314 -> 13:20 to 12:45', '2018-05-05 05:33:34', 6),
(85, 7, 'Flight DD9316 -> 11:00 to 13:20', '2018-05-05 05:33:47', 6),
(86, 7, 'Flight DD9324 -> 15:30 to 15:30', '2018-05-05 05:34:01', 6),
(87, 7, 'Flight DD9318 -> 17:25 to 17:25', '2018-05-05 05:34:17', 6),
(88, 14, 'Flight DD750 ->  05:40 to 07:00', '2018-05-05 05:35:10', 6),
(89, 14, 'Flight DD7502 -> 09:30 to 10:50', '2018-05-05 05:35:27', 6),
(90, 14, 'Flight DD7512 -> 10:15 to 11:50', '2018-05-05 05:35:40', 6),
(91, 14, 'Flight DD7504 -> 11:45 to 12:45', '2018-05-05 05:35:55', 6),
(92, 14, 'Flight DD7514 -> 15:05 to 16:25', '2018-05-05 05:36:11', 6),
(93, 14, 'Flight DD7520 -> 16:50 to 18:15', '2018-05-05 05:36:26', 6),
(94, 14, 'Flight DD7524 -> 20:35 to 22:00', '2018-05-05 05:36:46', 6),
(95, 17, 'Flight DD7910 -> 10:05 to 11:25', '2018-05-05 05:37:28', 6),
(96, 15, 'Flight DD7102 -> 06:00 to 07:25', '2018-05-05 05:37:56', 6),
(97, 15, 'Flight DD7104 -> 09:20 to 10:45', '2018-05-05 05:38:10', 6),
(98, 15, 'Flight DD7108 -> 13:10 to 14:35', '2018-05-05 05:38:25', 6),
(99, 15, 'Flight DD7112 -> 15:25 to 16:50', '2018-05-05 05:38:40', 6),
(100, 15, 'Flight DD7114 ->17:05 to 18:35', '2018-05-05 05:39:00', 6),
(102, 26, 'Flight DD7103 -> 07:55 to 09:20', '2018-05-05 05:49:15', 6),
(103, 26, 'Flight DD7105 -> 11:15 to 12:40', '2018-05-05 05:49:29', 6),
(104, 26, 'Flight DD7109  ->  15:05 to 16:30', '2018-05-05 05:49:46', 6),
(105, 26, 'Flight DD7113 -> 17:20 to 18:45', '2018-05-05 05:50:02', 6),
(106, 26, 'Flight DD7115 -> 19:20 to 20:45', '2018-05-05 05:50:16', 6),
(107, 27, 'Flight DD7911 -> 12:10 to 13:30', '2018-05-05 05:50:38', 6),
(108, 25, 'Flight DD7501 -> 07:50 to 09:15', '2018-05-05 05:51:03', 6),
(109, 25, 'Flight DD7503 -> 11:20 to 12:40', '2018-05-05 05:51:18', 6),
(110, 25, 'Flight DD7513 -> 12:45 to 14:10', '2018-05-05 05:51:33', 6),
(111, 25, 'Flight DD7505 -> 13:20 to 14:40', '2018-05-05 05:53:05', 6),
(112, 25, 'Flight DD7507 -> 15:25 to 16:45', '2018-05-05 05:54:06', 6),
(113, 25, 'Flight DD7515 -> 17:00 to 18:20', '2018-05-05 05:54:23', 6),
(114, 24, 'Flight DD9313 -> 09:00 to 10:00', '2018-05-05 05:54:41', 6),
(115, 24, 'Flight DD9315 -> 13:25 to 14:25', '2018-05-05 05:55:05', 6),
(116, 24, 'Flight DD9317 -> 14:55 to 15:55', '2018-05-05 05:55:26', 6),
(117, 24, 'Flight DD9325 -> 17:00 to 18:00', '2018-05-05 05:55:44', 6),
(118, 24, 'Flight DD9319 -> 9:05 to 20:05', '2018-05-05 05:55:59', 6),
(119, 23, 'Flight DD9201 -> 07:35 to 08:35', '2018-05-05 05:56:33', 6),
(120, 23, 'Flight DD9203 -> 11:50 to 12:50', '2018-05-05 05:56:48', 6),
(121, 23, 'Flight DD9211 -> 13:55 to 14:55', '2018-05-05 05:57:11', 6),
(122, 23, 'Flight DD9215 ->17:05 to 18:10', '2018-05-05 05:57:28', 6),
(123, 23, 'Flight DD9217 -> 18:35 to 19:35', '2018-05-05 05:57:49', 6),
(124, 22, 'Flight DD9807  -> 07:50 to 08:50', '2018-05-05 06:02:22', 6),
(125, 22, 'Flight DD9815 -> 12:20 to 13:10', '2018-05-05 06:02:56', 6),
(126, 22, 'Flight DD9821 -> 20:25 to 21:20', '2018-05-05 06:03:16', 6),
(127, 21, 'Flight DD8715 -> 09:20 to 10:30', '2018-05-05 06:03:43', 6),
(128, 21, 'Flight DD8719 -> 15:00 to 16:10', '2018-05-05 06:04:23', 6),
(129, 21, 'Flight DD8723 -> 18:25 to 19:40', '2018-05-05 06:05:08', 6),
(130, 20, 'Flight DD8303 -> 08:40 to 09:45', '2018-05-05 06:05:30', 6),
(131, 20, 'Flight DD8307 -> 10:45 to 11:50', '2018-05-05 06:06:32', 6),
(132, 20, 'Flight DD8313 -> 14:25 to 15:30', '2018-05-05 06:06:52', 6),
(134, 20, 'Flight DD8319 -> 18:10 to 19:15', '2018-05-05 06:08:12', 6),
(135, 20, 'Flight DD8325 -> 20:40 to 21:45', '2018-05-05 06:08:27', 6),
(136, 20, 'Flight DD8327 ->  21:55 to 23:00', '2018-05-05 06:08:47', 6),
(137, 20, 'Flight DD8317 -> 22:10 to 23:30', '2018-05-05 06:09:06', 6),
(138, 19, 'Flight DD7209 -> 07:50 to 09:00', '2018-05-05 06:09:44', 6),
(139, 19, 'Flight DD7211 -> 11:00 to 12:10', '2018-05-05 06:10:01', 6),
(140, 19, 'Flight DD7213 -> 14:20 to 15:30', '2018-05-05 06:10:16', 6),
(141, 19, 'Flight DD7217 -> 18:10 to 19:20', '2018-05-05 06:10:35', 6),
(142, 18, 'Flight DD7401 -> 09:20 to 10:40', '2018-05-05 06:12:51', 6),
(143, 18, 'Flight DD7407 -> 12:55 to 14:10', '2018-05-05 06:13:07', 6),
(144, 18, 'Flight DD7411 -> 18:05 to 19:25', '2018-05-05 06:13:22', 6);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `password`, `email`, `fname`, `gname`, `id_no`, `address`, `tel`) VALUES
(9, '928ea841d7dcccb74f64c4079ac8b07581eee72ef4157aebe26cd9ec685d4b50e810168405f77aa4d8bd3cdabcf61e04365399c9e7791084f8cdfd96103c2cd4NRgwgFEuZxATNVmEwrvSrcjXancFnw8Zw42SYJ6hSTU=', 'a@b.com', 'f', 'f', '0', '1', 1),
(10, '8e677d1acfeb5b279aad6489ea6daf3da2e2f7035bb1a79f055d6acd9454b92c3e9ead4f78d6cbbdd54c609b6ed3c544bab0d5dbe1d82ccbbbf3456a328fcaa9gXIUiuOIKhH/4Hhqn1EaYtNu95Y6PSLR/xlrp++A/R8=', 'q@hotmail.com', 'w', 'q', '1', 'qw', 12),
(11, '9e62cc2e945ca7168938283bb57f6952956d70e90b22330081b21948f0faff4ae0c22ab91112191b374dbf59f9c741c59353c4a231ec31c9ffb23b01fef693danj8upzeYZfuD4DXHh/QmPLwAwZiljDi9bzd2FSeocfg=', 'b@a.com', 'd', 'd', 'b1de0bd4e436bdc032508cf055b2ac99254eafb0cc9818c086ac54cb4c5fda47e2a01cec7cc2195641199d50cafc2be553d85c9d261f511997752f29e42a5d96/iXbJIDovLhwz0OnxtRWhIQik9cNY/q/gglhcdXyj28=', 'd', 4),
(12, 'd5765bac68d3b5672124bde37118109049f7791a6223fa9c23ab244c134d9f1b7a663f2965ab4889dd56ccda0d97d1ec5a574d72fbda6dbf355f5e796a2d18c4RKbcxO+/5Zu7vq9v9ncCQQNiR9X15RbQp5cKwfuNGmU=', 'a@b.com', 'a', 'a', 'c54d7d7943e2d62cc49f95df9aa127af9967a083e061cf45aa33a8edd5bef4c4132388dc571e86d590ea5563f7a1a40eb0ed13ea05b7cf1ef97787d5365248f1+H0c7PtKjgAhDY2C3NtyI4r8LmilqFodot/hIn3a16c=', 'v', 100000000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `flight_board`
--
ALTER TABLE `flight_board`
  ADD PRIMARY KEY (`flight_id`),
  ADD KEY `user_id_fk` (`user_id`);

--
-- Indexes for table `new_reserve`
--
ALTER TABLE `new_reserve`
  ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`promotion_id`),
  ADD KEY `user_id_fk` (`user_id`);

--
-- Indexes for table `reserve`
--
ALTER TABLE `reserve`
  ADD PRIMARY KEY (`reserve_id`),
  ADD KEY `user_id_fk` (`user_id`),
  ADD KEY `flight_id_fk` (`flight_id`) USING BTREE,
  ADD KEY `reserve_id_fk` (`reserve_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `flight_board`
--
ALTER TABLE `flight_board`
  MODIFY `flight_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `new_reserve`
--
ALTER TABLE `new_reserve`
  MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `promotion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reserve`
--
ALTER TABLE `reserve`
  MODIFY `reserve_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `reserve`
--
ALTER TABLE `reserve`
  ADD CONSTRAINT `webboard_id_fk` FOREIGN KEY (`flight_id`) REFERENCES `flight_board` (`flight_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
