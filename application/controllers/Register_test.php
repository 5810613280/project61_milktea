<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_test extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }

    public function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (strlen($email) <40){
                return true;
            }
        }
        else return false;
    }
    
    public function checkPassword($pass)
    {
        if(strlen($pass) > 8){
            return true;
        }
        else return false;
    }
    
    public function checkGivenName($name)
    {
        if (preg_match("/^[a-zA-Z]*$/", $name)) { // only contain A-Z a-z
                
            return true;
        }
        else return false;
    }
    
    public function checkFamilyName($name)
    {
        if (preg_match("/^[a-zA-Z]*$/", $name)) { // only contain A-Z a-z
                
            return true;
        }
        else return false;
    }
    
    public function checkIDno($idNO)
    {
        if (strlen($idNO)==13){
            if(preg_match("/^\d+$/", $idNO)) {
                return true;
            }
        }
        else return false;
    }
    
    public function checkTel($tel)
    {
        if (strlen($tel)==10){
            if(preg_match("/^\d+$/", $tel)) {
                return true;
            }
        }
        else return false;
    }
    
    
    public function index(){
        echo "<strong>Test Email Format</strong></br>";
        echo "abc@gmail.com";
        $info = "abc@gmail.com";
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Email Format</strong></br>";
        echo "abc.gmail.com";
        $info = "abc.gmail.com";
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Email Format</strong></br>";
        echo "abc@gmailcom";
        $info = "abc@gmailcom";
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Email Format</strong></br>";
        echo "abcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc@gmail.com";
        $info = "abcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc@gmail.com";
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Email Format</strong></br>";
        echo "NULL";
        $info = NULL;
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "1222231";
        $info = "1222231";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "11";
        $info = "11";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "aflksf;21323";
        $info = "aflksf;21323";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "aflksf;";
        $info = "aflksf;";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "%$%#%;";
        $info = "@$#^#^;";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "NULL";
        $info = NULL;
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Given Name</strong></br>";
        echo "siriporn";
        $info = "siriporn";
        $test = $this->checkGivenName($info);
        $expected_result = true;
        $test_name = "Test Given Name";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Given Name</strong></br>";
        echo "sirip8orn";
        $info = "sirip8orn";
        $test = $this->checkGivenName($info);
        $expected_result = true;
        $test_name = "Test Given Name";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Given Name</strong></br>";
        echo "12335";
        $info = "12335";
        $test = $this->checkGivenName($info);
        $expected_result = true;
        $test_name = "Test Given Name";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Given Name</strong></br>";
        echo "_dfkajj";
        $info = "_dfkajj";
        $test = $this->checkGivenName($info);
        $expected_result = true;
        $test_name = "Test Given Name";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Familly Name</strong></br>";
        echo "phasuk";
        $info = "phasuk";
        $test = $this->checkFamilyName($info);
        $expected_result = true;
        $test_name = "Test Familly Name";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Familly Name</strong></br>";
        echo "pha33suk";
        $info = "pha33suk";
        $test = $this->checkFamilyName($info);
        $expected_result = true;
        $test_name = "Test Familly Name";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Familly Name</strong></br>";
        echo "11111";
        $info = "11111";
        $test = $this->checkFamilyName($info);
        $expected_result = true;
        $test_name = "Test Familly Name";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Familly Name</strong></br>";
        echo "%$^#%fff";
        $info = "%$^#%fff";
        $test = $this->checkFamilyName($info);
        $expected_result = true;
        $test_name = "Test Familly Name";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test ID Card Number</strong></br>";
        echo "1234567890123";
        $info = "1234567890123";
        $test = $this->checkIDNo($info);
        $expected_result = true;
        $test_name = "Test ID Card Number";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test ID Card Number</strong></br>";
        echo "%$^#%ffferere";
        $info = "%$^#%ffferere";
        $test = $this->checkIDNo($info);
        $expected_result = true;
        $test_name = "Test ID Card Number";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test ID Card Number</strong></br>";
        echo "123";
        $info = "123";
        $test = $this->checkIDNo($info);
        $expected_result = true;
        $test_name = "Test ID Card Number";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Telephone Number</strong></br>";
        echo "0818181818";
        $info = "0818181818";
        $test = $this->checkTel($info);
        $expected_result = true;
        $test_name = "Test telephone number";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Telephone Number</strong></br>";
        echo "asr;lr'qlwrp";
        $info = "asr;lr'qlwrp";
        $test = $this->checkTel($info);
        $expected_result = true;
        $test_name = "Test telephone number";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Telephone Number</strong></br>";
        echo "08181818";
        $info = "08181818";
        $test = $this->checkTel($info);
        $expected_result = true;
        $test_name = "Test telephone number";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Telephone Number</strong></br>";
        echo "asdecfgrt6";
        $info = "asdecfgrt6";
        $test = $this->checkTel($info);
        $expected_result = true;
        $test_name = "Test telephone number";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        // TEST NULL ด้วย
        
    }
}
    