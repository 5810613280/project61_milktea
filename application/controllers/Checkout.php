<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load session library
        $this->load->library('session');
    }
    
    public function index()
    {
        $this->load->model('user_model');
        $this->load->model('order_model');
        $this->load->model('menu_model');
        
        // $order_id = $this->order_model->get_no_of_data() + 1;
        // echo $order_id;
        
        // $this->session->set_userdata('order_id', $order_id);
        $user_id = $this->session->userdata('logged_in');
        
        # create order
        $total_records = $this->order_model->get_no_of_order($user_id);
        $data['user'] = $this->user_model->get_info($user_id);
        $data['order_list'] = $this->order_model->get_order($user_id);
        $data['drink_list'] = $this->menu_model->get_all_drink($user_id);
        
        if ($total_records > 0)
        {
            $this->load->view('header_flight');
            $this->load->view('checkout_main', $data);
            $this->load->view('footer');
        } else {
            $this->load->view('header_flight');
            $this->session->set_flashdata('notify_message', 'Please add order before proceed to checkout.');
            $this->load->view('notify_menu');
            $this->load->view('footer');
        }
        
        // $data['info'] = $this->user_model->get_info($user_id);
            
        // $this->load->view('header_flight');
        // $this->load->view('select_address', $data);
        // $this->load->view('footer');
        
        // if ($this->order_model->insert_data($order_info)) {
            
        //     $data['info'] = $this->user_model->get_info($user_id);
            
        //     $this->load->view('header_flight');
        //     $this->load->view('select_address', $data);
        //     $this->load->view('footer');
            
        // }
    }
    
    public function payment($payment_method)
    {
        
        $this->load->model('order_model');
        
        $user_id = $this->session->userdata('logged_in');
        $data['payment'] = $payment_method;
        
        if ($this->order_model->update_order($user_id, $data)) {
                $this->order_model->complete_order($user_id);
                $this->session->set_flashdata('notify_message', 'Order Completed.');
            } else {
                $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            }
             // Load view
        $this->load->view('header');
        $this->load->view('notify');
        $this->load->view('footer');
    }
    
    public function delete($order_id)
    {
        $user_id = $this->session->userdata('logged_in');
        $this->load->model('order_model');

        if ($this->order_model->delete_order($order_id)) {
            $this->session->set_flashdata('notify_message', 'Delete drink successfully.');
        } else {
            $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
        }
        redirect('checkout');
    }
    
}