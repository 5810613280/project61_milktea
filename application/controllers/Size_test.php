<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Size_test extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }
    
    private function testsize($info){
        $this->load->model('list_model');
        $total_records = $this->list_model->get_size($info[0],$info[1]);
        return $total_records; 
    }
    
    public function index(){
        echo "<strong>Test size for user_id : 1, order_id : 9</strong></br>";
        echo "Expected size M";
        $info =array(1,9);
        $test = $this->testsize($info);
        $expected_result = "M";
        $test_name = "Test size selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test size for user_id : 11, order_id : 29</strong></br>";
        echo "Expected size S";
        $info =array(11,29);
        $test = $this->testsize($info);
        $expected_result = "S";
        $test_name = "Test size selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test size for user_id : 21, order_id : 33</strong></br>";
        echo "Expected size M";
        $info =array(21,33);
        $test = $this->testsize($info);
        $expected_result = "M";
        $test_name = "Test size selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test size for user_id : 41, order_id : 46</strong></br>";
        echo "Expected size M";
        $info =array(41,46);
        $test = $this->testsize($info);
        $expected_result = "M";
        $test_name = "Test size selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
    }
}