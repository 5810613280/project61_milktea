<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topping_test extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }
    
    private function testtopping($info){
        $this->load->model('list_model');
        $total_records = $this->list_model->get_topping($info[0],$info[1]);
        return $total_records; 
    }
    
    public function index(){
        echo "<strong>Test topping for user_id : 1, order_id : 9</strong></br>";
        echo "Expected topping Brown Sugar Jelly";
        $info =array(1,9);
        $test = $this->testtopping($info);
        $expected_result = "Brown Sugar Jelly";
        $test_name = "Test topping selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test topping for user_id : 11, order_id : 29</strong></br>";
        echo "Expected topping Black Pearl";
        $info =array(11,29);
        $test = $this->testtopping($info);
        $expected_result = "Black Pearl";
        $test_name = "Test topping selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test topping for user_id : 21, order_id : 33</strong></br>";
        echo "Expected topping Brown Sugar Jelly";
        $info =array(21,33);
        $test = $this->testtopping($info);
        $expected_result = "Brown Sugar Jelly";
        $test_name = "Test topping selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test topping for user_id : 41, order_id : 46</strong></br>";
        echo "Expected topping Crystal Boba";
        $info =array(41,46);
        $test = $this->testtopping($info);
        $expected_result = "Crystal Boba";
        $test_name = "Test topping selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
    }
}