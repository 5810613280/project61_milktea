<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load form and url helper
        $this->load->helper(['form', 'url']);

        // Load session library
        $this->load->library('session');
    }
    
    public function index()
    {
        $this->load->model('menu_model');

        $total_records = $this->menu_model->get_no_of_data();
        $data['posts'] = $this->menu_model->get_all_drink();
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('drink_id', 'drink_id','required');
        $this->form_validation->set_rules('sweet','sweet','required');
        $this->form_validation->set_rules('topping','topping','required');
        $this->form_validation->set_rules('size', 'size','required');
        $this->form_validation->set_rules('amount','amount','required');
        
        if ($this->form_validation->run() == FALSE)
        {
            if ($total_records > 0) 
            {
                $this->load->view('header_flight');
                $this->load->view('menu_view', $data);
                $this->load->view('footer');
            } else {
                $this->load->view('header_flight');
                $this->load->view('footer');
            }
        } else {
            $this->load->model('order_model');
            $user_id = $this->session->userdata('logged_in');
        
            $submit_order['user_id'] = $user_id;
            $submit_order['sweet'] = $this->input->post('sweet', TRUE);
            $submit_order['topping'] = $this->input->post('topping', TRUE);
            $submit_order['size'] = $this->input->post('size', TRUE);
            $submit_order['drink_id'] = $this->input->post('drink_id', TRUE);
            $submit_order['amount'] = $this->input->post('amount', TRUE);
            
            if ($this->order_model->insert_data($submit_order)) {
                $this->session->set_flashdata('notify_message', 'Order successfully.');
            } else {
                $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            }
             // Load view
            $this->load->view('header');
            $this->load->view('notify_menu');
            $this->load->view('footer');
        }
        
    }
    
    
}