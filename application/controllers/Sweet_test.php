<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sweet_test extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }
    
    private function testSweet($info){
        $this->load->model('list_model');
        $total_records = $this->list_model->get_sweet($info[0],$info[1]);
        return $total_records; 
    }
    
    public function index(){
        echo "<strong>Test Sweet for user_id : 1, order_id : 9</strong></br>";
        echo "Expected sweet 75%";
        $info =array(1,9);
        $test = $this->testSweet($info);
        $expected_result = "75%";
        $test_name = "Test sweet selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Sweet for user_id : 11, order_id : 29</strong></br>";
        echo "Expected sweet 100%";
        $info =array(11,29);
        $test = $this->testSweet($info);
        $expected_result = "100%";
        $test_name = "Test sweet selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Sweet for user_id : 21, order_id : 33</strong></br>";
        echo "Expected sweet 100%";
        $info =array(21,33);
        $test = $this->testSweet($info);
        $expected_result = "100%";
        $test_name = "Test sweet selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Sweet for user_id : 41, order_id : 46</strong></br>";
        echo "Expected sweet 25%";
        $info =array(41,46);
        $test = $this->testSweet($info);
        $expected_result = "25%";
        $test_name = "Test sweet selection function";
        echo $this->unit->run($test,$expected_result,$test_name);
        
    }
}