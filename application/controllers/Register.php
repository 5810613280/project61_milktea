<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Load form and url helper
        $this->load->helper(['form','url']);

        $this->load->library('session');
    }

    public function index()
    {

        // Load form validation
        $this->load->library('form_validation');
        $this->load->library('encryption');

//        $this->form_validation->set_rules('username','username','required|callback_check_username');
        $this->form_validation->set_rules('password', 'password','required');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('gname','gname','required');
        $this->form_validation->set_rules('fname','fname','required');
        $this->form_validation->set_rules('id_no','id_no','required|exact_length[13]');
        $this->form_validation->set_rules('address','address','required');
        $this->form_validation->set_rules('tel','tel','required|exact_length[10]');
        $this->form_validation->set_rules('cpassword','Confirm password','trim|required|matches[password]');


        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('header');
            $this->load->view('register_view');
            $this->load->view('footer');
        } else {
            $this->load->model('user_model');
//            echo 3;
//            $submit_data['username'] = $this->input->post('username', TRUE);

            $plain_password = $this->input->post('password', TRUE);
            $cipher_password = $this->encryption->encrypt($plain_password);

            $plain_id = $this->input->post('id_no', TRUE);
            $cipher_id = $this->encryption->encrypt($plain_id);



//            $submit_data['password'] = $this->input->post('password', TRUE);
//            $submit_data['fullname'] = $this->input->post('fullname', TRUE);
            $submit_data['password'] = $cipher_password;
            $submit_data['email'] = $this->input->post('email', TRUE);
            $submit_data['fname'] = $this->input->post('fname', TRUE);
            $submit_data['gname'] = $this->input->post('gname', TRUE);
            $submit_data['id_no'] = $cipher_id;
            $submit_data['address'] = $this->input->post('address', TRUE);
            $submit_data['tel'] = $this->input->post('tel', TRUE);



            if ($this->user_model->insert_data($submit_data)) {
                $this->session->set_flashdata('notify_message', 'Sign up successfully.');
            } else {
                $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            }

            // Load view
            $this->load->view('header');
            $this->load->view('notify');
            $this->load->view('footer');

        }
    }

    public function check_username($email)
    {
        $this->load->model('user_model');
        $id = $this->user_model->get_id($email);
        if ($id == FALSE)
        {
            return TRUE;
        }
        $this->form_validation->set_message('check_username', 'This email already exists.');
        return FALSE;
    }



}