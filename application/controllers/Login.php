<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Load form and url helper
        $this->load->helper(array('form','url'));
    }

    public function index()
    {
        // Load form validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password','trim|required|callback_check_database');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('header');
            $this->load->view('login_view');
            $this->load->view('footer');
        }
        else
        {
            redirect('main', 'refresh');
        }
    }
    
    public function validateEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (strlen($email) <40){
                return true;
            }
        }
        else return false;
    }

    public function check_database($password)
    {
        if (empty($password)) {
            $this->form_validation->set_message('check_database', 'The Password field is required.');
            return FALSE;
        }

        $this->load->model('user_model');

        $email = $this->input->post('email');

        if ($this->user_model->login($email,$password))
        {
            $id = $this->user_model->get_id($email);
            $this->session->set_userdata('email', $email);
            $this->session->set_userdata('logged_in',$id);
            return TRUE;
        }
        $this->form_validation->set_message('check_database', 'Invalid email or password.');
        return FALSE;
    }

}