<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_test extends CI_Controller {
    
    protected $login_test;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }
    
    public function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (strlen($email) <40){
                return true;
            }
        }
        else return false;
    }
    
    public function checkPassword($pass)
    {
        if(strlen($pass) > 8){
            return true;
        }
        else return false;
    }
    
    public function index()
    {
        echo "<strong>Test Email Format</strong></br>";
        echo "abc@gmail.com";
        $info = "abc@gmail.com";
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Email Format</strong></br>";
        echo "abc.gmail.com";
        $info = "abc.gmail.com";
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Email Format</strong></br>";
        echo "abc@gmailcom";
        $info = "abc@gmailcom";
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Email Format</strong></br>";
        echo "abcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc@gmail.com";
        $info = "abcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc@gmail.com";
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Email Format</strong></br>";
        echo "NULL";
        $info = NULL;
        $test = $this->checkEmail($info);
        $expected_result = true;
        $test_name = "Test email format";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "1222231";
        $info = "1222231";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "11";
        $info = "11";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "aflksf;21323";
        $info = "aflksf;21323";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "aflksf;";
        $info = "aflksf;";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "%$%#%;";
        $info = "@$#^#^;";
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
        
        echo "<strong>Test Password Format</strong></br>";
        echo "NULL";
        $info = NULL;
        $test = $this->checkPassword($info);
        $expected_result = true;
        $test_name = "Test Password";
        echo $this->unit->run($test,$expected_result,$test_name);
    }
    
}