<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Load form and url helper
        $this->load->helper(array('form','url'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->session->sess_destroy();
        redirect('/');
    }

}