<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load session library
        $this->load->library('session');
    }
    
    public function index()
    {
        $this->load->model('user_model');
        $this->load->model('order_model');
        
        $order_id = $this->order_model->get_no_of_data() + 1;
        // echo $order_id;
        
        $this->session->set_userdata('order_id', $order_id);
        $user_id = $this->session->userdata('logged_in');
        
        # create order
        $order_info['user_id'] = $user_id;
        
        $data['info'] = $this->user_model->get_info($user_id);
            
        $this->load->view('header_flight');
        $this->load->view('select_address', $data);
        $this->load->view('footer');
        
        // if ($this->order_model->insert_data($order_info)) {
            
        //     $data['info'] = $this->user_model->get_info($user_id);
            
        //     $this->load->view('header_flight');
        //     $this->load->view('select_address', $data);
        //     $this->load->view('footer');
            
        // }
    }
    
    public function update($user_id)
    {
        // $this->check_login();
        $this->load->model('user_model');
        
        $this->form_validation->set_rules('address', 'address', 'required');
        $user_id = $this->session->userdata('logged_in');
        $data['info'] = $this->user_model->get_info($user_id);
        
        if ($this->form_validation->run() == FALSE) {
        $this->load->view('header_flight');
        $this->load->view('address_update_view', $data);
        $this->load->view('footer');
        } else {
            $submit_data['address'] = $this->input->post('address', TRUE);
        
            if ($this->user_model->update_address($user_id, $submit_data)) {
                $this->session->set_flashdata('notify_message', 'Address successfully edited.');
            } else {
                $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            }

            redirect('address');
        }
        
        
    }
}