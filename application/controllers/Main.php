<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
    }

    public function index()
    {
        $this->load->view('header_index');
        $this->load->view('index');
        $this->load->view('footer_index');
    }
    
    public function about()
    {
        $this->load->view('header_index');
        $this->load->view('about');
        $this->load->view('footer_index');
    }
}