<?php

class register_test extends TestCase
{
    public function test_index()
    {
        $output = $this->request('GET', 'register/index');
        $this->assertContains(
            '<h1 class="message">Sign Up</h1>', $output);
    }

}