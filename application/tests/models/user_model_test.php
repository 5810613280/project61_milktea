<?php

class user_model_test extends TestCase
{
	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('user_model');
		$this->obj = $this->CI->user_model;
	}

	public function test_get_list_of_database()
	{
		$expected = [
		    1 => 'user_id',
			2 => 'password',
			3 => 'email',
			4 => 'fname',
			5 => 'gname',
			6 => 'id_no',
			7 => 'address',
			8 => 'tel'
		];
		$list = $this->obj->get_list_of_database();
		$counts = [0,1,2,3,4,5,6,7];
		foreach ($counts as $count) {
			$this->assertEquals($expected[$count+1], $list[$count]);
		}
	}
	
	public function test_get_info()
	{
	    $expected = [
	        'user_id' => '9',
            'email' => 'a@b.com',
            'fname'  => 'f',
            'gname'  => 'f',
            'tel'    => '1',
            'address' => 'abcde',
            ];
        $output = $this->obj->get_info(9);
        $temps = ['user_id', 'email', 'fname', 'gname', 'tel', 'address'];
        foreach($temps as $temp) {
            $this->assertEquals($expected[$temp], $output[$temp]);
        }
	}
	
	public function test_get_id()
	{
	    $this->assertEquals($this->obj->get_id('a@b.com'), 9);
	}
	
	public function test_login()
	{
	    $this->assertTrue($this->obj->login('a@b.com','1234'));
	}
}