<?php


class order_model_test extends TestCase
{
	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('order_model');
		$this->obj = $this->CI->order_model;
	}
	
	public function test_get_list_of_database()
	{
		$expected = [
		    1 => 'user_id',
			2 => 'order_id',
			3 => 'drink_id',
			4 => 'size',
			5 => 'sweet',
			6 => 'topping',
			7 => 'amount',
			8 => 'payment',
			9 => 'status'
		];
		$list = $this->obj->get_list_of_database();
		$counts = [0,1,2,3,4,5,6,7,8];
		foreach ($counts as $count) {
			$this->assertEquals($expected[$count+1], $list[$count]);
		}
	}
}