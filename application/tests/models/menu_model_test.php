<?php


class menu_model_test extends TestCase
{
	public function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('menu_model');
		$this->obj = $this->CI->menu_model;
	}
	
	public function test_get_list_of_database()
	{
		$expected = [
		    1 => 'drink_id',
			2 => 'drink',
			3 => 'price'
		];
		$list = $this->obj->get_list_of_database();
		$counts = [0,1,2];
		foreach ($counts as $count) {
			$this->assertEquals($expected[$count+1], $list[$count]);
		}
	}
	
	public function test_get_drink_name()
	{
	    $this->assertEquals($this->obj->get_drink_name(1), 'chanom');
	}
	
// 	public function test_get_all_drink_name()
// 	{
// 	    $expected = [
// 		    1 => 'chanom',
// 			2 => 'greentea',
// 			3 => 'redtea'
// 		];
// 		$lists = $this->obj->get_all_drink_name();
// 		$count = 1;
// 		foreach ($lists as $list) {
// 			$this->assertEquals($expected[$count], $list->drink);
// 			$count++;
// 	    }
// 	}
	
}