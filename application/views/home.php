<!-- HOME -->
<main class="site-main">

    <?php foreach ($posts as $item) : ?>
        <article class="aa">
            <div class="aa-title">
                <?php $post_id = $item['post_id']; ?>
                <h1><a href="<?= base_url("main/show_post/$post_id"); ?>"><?= $item['post_title']; ?></a></h1>
            </div>
            <div class="aa-images">
                <?php $img_name = $item['img_name']; ?>
                <img src="<?= base_url("public/images/$img_name"); ?>">
            </div>
            <div class="aa-home-content">
                <p><?= $item['post_content'] ?></p>

            </div>
            <div class="read-more">
                <a href="<?= base_url("main/show_post/$post_id"); ?>">Readmore»</a>
            </div>
            <span class="date"><?= date('d/m/Y H:i:s', strtotime($item['post_time'])); ?></span>
        </article>
    <?php endforeach; ?>

    <div class="pagination">
        <?= $pagination_link; ?>
    </div>

</main>
<!-- /HOME -->