<!-- HOME -->

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 3px solid #252526;
        text-align: left;
        padding: 8px;
        color: #252526;
    }

    input[type=submit] {
        background:#252526;
        border-radius: 10px;
        width: 100%;
        border:#f3f0eb;
        color: #fff;
    }
    .proc {
        background:#252526;
        border-radius: 10px;
        width: 100%;
        height: 30px;
        border:#f3f0eb;
        color: #fff;
        text-align: center;
        color: white;
        margin-right: 30px;
    }
    a:link, a:visited {
        color: white;
    }
</style>

<main class="site-main-promotion">
    <?php echo validation_errors('<div class="error">', '</div>'); ?>
<!--    <h2 class="message" >-Menu-</h2>-->
    <?= form_open('menu', 'class=""'); ?>

    <table>
        <?php foreach ($posts as $item) : ?>
            <tr>
                <td><?= $item['drink'] ?></td>
                <td><?= $item['price'] ?></td>
                <td><input type="radio" name="drink_id" value="<?=$item['drink_id']?>"></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <div class="site-main-promotion-drop">
        <?php 
        $options = array(
            NULL =>  'sweet',
            '100%'         => '100%',
            '75%'           => '75%',
            '50%'         => '50%',
            '25%'        => '25%',
        );
        echo form_dropdown('sweet', $options, ''); 
        ?>
        
        <?php 
        $options = array(
            NULL =>  'topping',
            'Black Pearl'         => 'Black Pearl',
            'Brown Sugar Jelly'           => 'Brown Sugar Jelly',
            'Crystal Boba'         => 'Crystal Boba',
            'Green Apple Jelly'        => 'Green Apple Jelly',
        );
        echo form_dropdown('topping', $options, ''); 
        ?>
    
        <?php 
        $options = array(
            NULL =>  'size',
            'S'         => 'S',
            'M'           => 'M',
            'L'         => 'L'
        );
        echo form_dropdown('size', $options, ''); 
        ?>
    
        <?php 
        $options = array(
            NULL =>  'amount',
            '1'         => '1',
            '2'           => '2',
            '3'         => '3',
            '4'         => '4',
            '5'           => '5',
            '6'         => '6'
        );
        echo form_dropdown('amount', $options, ''); 
        ?>
    
    </div>
    <div>
        <?= form_submit('', 'Add order!'); ?>
    </div>
    <div class="proc">
            <a href="<?= base_url("checkout"); ?>">Proceed to checkout</a>
    </div>
    <?= form_close(); ?>

</main>
<!-- /HOME -->