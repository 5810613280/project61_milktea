<!-- CONTENT -->
<main class="main-area">

    <h1 class="message">Sign Up</h1>
    <?php echo validation_errors('<div class="error">','</div>'); ?>

    <?= form_open('register', 'class=""'); ?>

<!---->
<!--    --><?php
//    echo form_label('Username: ','username');
//    $data = ['name' => 'username', 'id' => 'username'];
//    echo form_input($data);
//    ?>

    <?php
    echo form_label('Email: ','email');
    $data = ['name' => 'email', 'id' => 'email'];
    echo form_input($data);
    ?>

    <?php
    echo form_label('Password: ','password');
    $data = ['name' => 'password', 'id' => 'password'];
    echo form_password($data);
    ?>

    <?php
    echo form_label('Confirm password: ','cpassword');
    $data = ['name' => 'cpassword', 'id' => 'cpassword'];
    echo form_password($data);
    ?>


    <?php
    echo form_label('Given Name: ','gname');
    $data = ['name' => 'gname', 'id' => 'gname'];
    echo form_input($data);
    ?>

    <?php
    echo form_label('Family Name: ','fname');
    $data = ['name' => 'fname', 'id' => 'fname'];
    echo form_input($data);
    ?>

    <?php
    echo form_label('ID card number: ','id_no');
    $data = ['name' => 'id_no', 'id' => 'id_no'];
    echo form_input($data);
    ?>

    <?php
    echo form_label('Address: ','address');
    $data = ['name' => 'address', 'id' => 'address'];
    echo form_input($data);
    ?>


    <?php
    echo form_label('Tel: ','tel');
    $data = ['name' => 'tel', 'id' => 'tel'];
    echo form_input($data);
    ?>


<!---->
<!--    --><?php
//    echo form_label('Display name: ','fullname');
//    $data = ['name' => 'fullname', 'id' => 'fullname'];
//    echo form_input($data);
//    ?>


    <div>
        <?= form_submit('', 'Submit!'); ?>
    </div>
    <div>
        <?= form_reset('', 'Reset'); ?>
    </div>

    <?= form_close(); ?>

</main>
<!-- /CONTENT -->