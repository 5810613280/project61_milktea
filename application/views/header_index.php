<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Final_Project</title>
    <link href="https://fonts.googleapis.com/css?family=Cookie|Fira+Sans|Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("public/css") ;?>/index.css">
    <link rel="stylesheet" href="<?= base_url("public/css") ;?>/index.css" media="screen">
    <link href="https://fonts.googleapis.com/css?family=Taviraj:300" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
</head>
<body>

<div class="container-index">

    <header class="site-header-index">
        <div class="site-header-title"> 
            <h2>BUBBLE TEA♡</h2>
        </div>
       
        <nav>
                <ul>
                    <li><a href="<?= base_url(); ?>"><img src="<?=base_url("public/images");?>/iconhomeblack.png"></a></li>
                    <?php if ($this->session->logged_in) : ?>
                        <li><a href="<?= base_url('logout'); ?>" onclick="return confirm('Are you sure you want to logout?')"><img src="<?=base_url("public/images");?>/iclogout.png"></a></li>
                    <?php else: ?>
                        <li><a href="<?=base_url('login'); ?>"><img src="<?=base_url("public/images");?>/iclogin.png"></a></li>
                    <?php endif; ?>
                    <li><a href="<?=base_url('main/about'); ?>">About</a></li>
                </ul>
                <ul>
                    <?php if ($this->session->logged_in) : ?>
                        <li><a href="<?=base_url('Address'); ?>">START MY ORDER</a></li>
                    <?php else: ?>
                        <li><a href="<?=base_url('login'); ?>">START MY ORDER</a></li>
                    <?php endif; ?>
                </ul>
        </nav>
    </header>