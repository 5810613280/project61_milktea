<style>
    a:link, a:visited {
        color: white;
    }
</style>
<main class="main-area">
    <?php $address =  $info['address'] ?>
    <?php $user_id =  $info['user_id'] ?>
    <h1 class="message">Add new address</h1>

    <?= form_open("address/update/$user_id", 'class=""'); ?>
    
    <?php
    echo form_label('Address: ','address');
    $data = ['name' => 'address', 'id' => 'address', 'cols' => '5', 'rows' => '5', 'value' => set_value('address'), 'value'=>$address];
    echo form_textarea($data);
    ?>

    <div>
        <?= form_submit('', 'Submit'); ?>
    </div>
    <div>
        <?= form_reset('', 'Reset'); ?>
    </div>
    <div class="input_cancle">
        <a href="<?= base_url("address/"); ?>">Cancel</a>
    </div>

    <?= form_close(); ?>

</main>