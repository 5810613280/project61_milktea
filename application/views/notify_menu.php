<!-- CONTENT -->
<main class="site-main">

    <div class="message">
        <h2><?= $_SESSION['notify_message']; ?></h2>
    </div>

    <div class="site-main-subbody">
            <h3><a href="<?= base_url("menu"); ?>">Add more drink</a></h3>
    </div>
    
    <div class="site-main-subbody">
            <h3><a href="<?= base_url("checkout"); ?>">Proceed to checkout</a></h3>
    </div>

</main>
<!-- /CONTENT -->