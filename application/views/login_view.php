<!-- CONTENT -->
<main class="site-main">

    <h2 class="message">Login</h2>
    <?php echo validation_errors('<div class="error">', '</div>'); ?>

    <?= form_open('login', 'class=""'); ?>

        <?php
        echo form_label('Email: ', 'email');
        $data = ['name' => 'email', 'id' => 'email'];
        echo form_input($data);
        ?>

        <?php
        echo form_label('Password: ', 'password');
        $data = ['name' => 'password', 'id' => 'password'];
        echo form_password($data);
        ?>

    <div>
        <?= form_submit('submit', 'Login'); ?>
    </div>
    <div>
        <?= form_reset('reset', 'Reset'); ?>
    </div>
    <?= form_close(); ?>

<!--    <div class="newuser">-->
    <h3 class="newuser">New user?
        <a href="<?= base_url('Register'); ?>">Sign up!!</a>
    </h3>
<!--    </div>-->
</main>
<!-- /CONTENT -->

