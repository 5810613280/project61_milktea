<!-- CONTENT -->
<style>
    table.one {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td.oness, th.oness {
        border: 3px solid #252526;
        text-align: left;
        padding: 8px;
        color: #252526;
    }
    td.last {
        border-bottom: none;
        border-left: none;
        border-right:none;
    }
    .proc {
        background:#252526;
        border-radius: 10px;
        width: 100%;
        height: 30px;
        border:#f3f0eb;
        color: #fff;
        text-align: center;
        color: white;
        margin-right: 30px;
    }
    table.two {
        margin-top: 40px;
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    td.twos {
        padding: 8px;
        color: #252526;
    }
    a:link.twos, a:visited.twos{
        color: white;
    }
</style>

<main class="site-main">
    <div class="site-main-body">
        <div class="site-main-subbody-title">
            <h1>Checkout</h1>
            <h3>Delivery To: <?php echo $user['address'] ?></h3>
        </div>
        <table class="one">
        <?php $totalprice = 0 ?>
        <?php $cash = 'cash'; ?>
        <?php $credit = 'credit'; ?>
        <tr>
            <th class="oness">Menu</th>
            <th class="oness">Sweet</th>
            <th class="oness">Topping</th>
            <th class="oness">Size</th>
            <th class="oness">Amount</th>
            <th class="oness">Price</th>
            <th class="oness">Action</th>
        </tr>
        <?php foreach ($order_list as $item) : ?>
        <?php $order_id = $item['order_id']; ?>
        <tr>
            <td class="oness"><?php foreach ($drink_list as $drink) : ?>
            <?php if ($item['drink_id'] == $drink['drink_id']): ?>
            <?= $drink['drink'] ?>
            <?php $totalprice += $drink['price']*$item['amount']; ?>
            <?php break; ?>
            <?php endif; ?>
            <?php endforeach; ?></td>
            <td class="oness"><?= $item['sweet'] ?></td>
            <td class="oness"><?= $item['topping'] ?></td>
            <td class="oness"><?= $item['size'] ?></td>
            <td class="oness"><?= $item['amount'] ?></td>
            <td class="oness">
                <?php foreach ($drink_list as $drink) : ?>
                <?php if ($item['drink_id'] == $drink['drink_id']): ?>
                <?php $this_price = $drink['price']*$item['amount']; ?>
                <?= $this_price ?>
                <?php break; ?>
                <?php endif; ?>
                <?php endforeach; ?>
            </td>
            <td class="oness"><a href="<?= base_url("checkout/delete/$order_id"); ?>" onclick="return confirm('Are you sure?')" ><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td class="last"></td>
            <td class="last"></td>
            <td class="last"></td>
            <td class="last"></td>
            <td class="oness">Total</td>
            <td class="oness"><?=$totalprice ?><?= ' Baht' ?></td>
            <td class="last"></td>
        </tr>
    </table>
    </div>
    
    <table class="two">
        <tr>
            <td classs="twos">
                <div class="proc">
                    <a class="twos" href="<?= base_url("menu"); ?>">Add more drink</a>
                </div>
            </td>
            <td classs="twos">
                <div class="proc">
                    <a class="twos" href="<?= base_url("checkout/payment/$credit"); ?>">Pay by credit card</a>
                </div>
            </td>
            <td classs="twos">
                <div class="proc">
                    <a class="twos" href="<?= base_url("checkout/payment/$cash"); ?>">Pay by cash</a>
                </div>
            </td>
        </tr>
    </table>
    
</main>
<!-- /CONTENT -->