<!DOCTYPE html>
<html>
<body>

<h1>My First Google Map</h1>

<div id="googleMap" style="width:100%;height:400px;"></div>
<p id="test"></p>
<script>

var markers = [];
var lat;
var lng;

function myMap() {
var map= {
    center:new google.maps.LatLng(13.736717,100.523186),
    zoom:10,
};
var map=new google.maps.Map(document.getElementById("googleMap"),map);

google.maps.event.addListener(map, 'click', function(event) {
    deleteMarkers(event);
    placeMarker(event.latLng, map);
});
}


function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }


function placeMarker(location, map) {
    var marker = new google.maps.Marker({
        position: location, 
        map: map
    });
    markers.push(marker);
    lat = markers[0].getPosition().lat();
	lng = markers[0].getPosition().lng();
}

function deleteMarkers() {
        clearMarkers();
        markers = [];
    
}

function clearMarkers() {
        setMapOnAll(null);
      }


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFhR8zOFqWRu5F7QAmJ8eQG6g9rC9agLk&callback=myMap"></script>

</body>
</html>