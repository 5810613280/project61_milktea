<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Menu_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_all_drink()
    {
        $query = $this->db->get('menu');
        
        if ($query->num_rows()>0) {
            foreach ($query->result() as $item) {
                $data[] = [
                    'drink_id' => $item->drink_id,
                    'drink' => $item->drink,
                    'price'  => $item->price
                ];
            }
            return $data;
        }
        return FALSE;
    }
    
    public function get_all_drink_name()
    {
        $query = $this->db->get('menu');
        if ($query->num_rows()>0) {
            foreach ($query->result() as $item) {
                $data[] = [
                    'drink' => $item->drink,
                ];
            }
            return $data;
        }
        return FALSE;
    }
    
    public function get_drink_name($drink_id)
    {
        $this->db->where('drink_id', $drink_id);
        $query = $this->db->get('menu');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data = $row->drink;
            return $data;
        }
        return FALSE;
    }
    
    public function get_no_of_data()
    {
        return $this->db->count_all_results('menu');
    }
    
    public function get_list_of_database()
    {
        return $this->db->list_fields('menu');
    }

}