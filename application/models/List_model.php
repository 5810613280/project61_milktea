<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class List_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }
    
    public function insert_data($data)
    {
        return $this->db->insert('order_list',$data);
    }
    
    public function get_no_of_data()
    {
        return $this->db->count_all_results('order_list');
    }
    
    public function get_sweet($id,$order_id)
    {
        $this->db->where('order_id', $order_id);
        $this->db->where('id', $id);
        $query = $this->db->get('order_list');
        
        if ($query->num_rows()>0){
            $row = $query->row();
            $sweet_result = $row->sweet;
            return $sweet_result;
        }
        return FALSE;
    }
    
    public function get_topping($id,$order_id)
    {
        $this->db->where('order_id', $order_id);
        $this->db->where('id', $id);
        $query = $this->db->get('order_list');
        
        if ($query->num_rows()>0){
            $row = $query->row();
            $topping_result = $row->topping;
            return $topping_result;
        }
        return FALSE;
    }

    public function get_size($id,$order_id)
    {
        $this->db->where('order_id', $order_id);
        $this->db->where('id', $id);
        $query = $this->db->get('order_list');
        
        if ($query->num_rows()>0){
            $row = $query->row();
            $size_result = $row->size;
            return $size_result;
        }
        return FALSE;
    }

}