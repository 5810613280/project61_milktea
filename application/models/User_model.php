<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class User_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function insert_data($data)
    {
        return $this->db->insert('user',$data);
    }

    public function login($email, $password)
    {
        $this->load->library('encryption');

        $this->db->where('email', $email);
        $query = $this->db->get('user');
        if ($query->num_rows()>0)
        {
            $row = $query->row();
            $pass = $row->password;
            $cpass = $this->encryption->decrypt($pass);

            if ($cpass == $password)
            {
                return TRUE;
            }

        }
        return FALSE;
    }

    public function get_id($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('user');

        if ($query->num_rows()>0){
            $row = $query->row();
            $id = $row->user_id;
            return $id;
        }
        return FALSE;
    }

    public function get_info($user_id)
    {
        $this->load->library('encryption');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user');
        if ($query->num_rows()>0) {
            $item = $query->row();
            $plain_id = $this->encryption->decrypt($item->id_no);
            $data = [
                'user_id' => $item->user_id,
                'email' => $item->email,
                'fname'  => $item->fname,
                'gname'  => $item->gname,
                'tel'    => $item->tel,
                'address' => $item->address,
            ];
            return $data;
        }
        return false;
    }

    public function update_address($user_id, $data)
    {
        $this->db->where('user_id', $user_id);
        return $this->db->update('user', $data);
    }
    
    public function get_list_of_database()
    {
        return $this->db->list_fields('user');
    }

}