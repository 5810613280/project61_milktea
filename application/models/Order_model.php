<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Order_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function insert_data($data)
    {
        return $this->db->insert('order',$data);
    }
    
    public function get_no_of_data()
    {
        return $this->db->count_all_results('order');
    }
    
    public function get_no_of_order($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'pending');
        $query = $this->db->get('order');
        
        return $query->num_rows();
    }
    
    public function get_order($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'pending');
        $query = $this->db->get('order');
        if ($query->num_rows()>0)
        {
            foreach ($query->result() as $item) {
                $data[] = [
                'user_id' => $item->user_id,
                'order_id' => $item->order_id,
                'drink_id' => $item->drink_id,
                'status' => $item->status,
                'sweet' => $item->sweet,
                'topping' => $item->topping,
                'size' => $item->size,
                'amount' => $item->amount,
                'payment' => $item->payment
                ];
            }
            return $data;
        }
        return FALSE;
    }
    
    public function complete_order($user_id)
    {
        $status['status'] = 'complete';
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'pending');
        return $this->db->update('order', $status);
    }
    
    public function update_order($user_id, $data)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'pending');
        return $this->db->update('order', $data);
    }

    public function delete_order($order_id)
    {
        $this->db->where('order_id', $order_id);
        return $this->db->delete('order');
    }
    
    public function get_list_of_database()
    {
        return $this->db->list_fields('order');
    }

}