<?php

class User_test extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('user_model');
        $this->obj = $this->CI->user_model;
    }
    
    public function test_get_category_list()
    {
        $expected = [
            1 => 'user_id',
            2 => 'email',
            3 => 'fname',
            4 => 'gname',
            5 => 'tel',
            6 => 'address'
        ];
        $list = $this->obj->get_list_of_database();
        foreach ($list as $category) {
            $this->assertEquals($expected[$category->id], $category->name);
        }
    }


}