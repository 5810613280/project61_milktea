#Chanom Application

##Heroku URL: https://chanomeiei.herokuapp.com/

##Iteration 1
Our web appliction doesn't connect to database yet, so it doesn't have test.

As a customer

I want the system to remember my location that I have ever ordered

So that I can choose my location that I want
#
As a customer

I want the system remember my information about name

So that I don�t have to resubmit my name again
#
As a customer

I want the system remember my information about e-mail

So that I don�t have to resubmit my name e-mail again
#
As a customer

I want the system remember my information about phone number

So that I don�t have to resubmit my name phone number again

##Iteration 2

As a customer

I want to be able to select how sweet of the milk tea

So that I can choose how sweet of the milk tea I like

- User stories sweet selection test : https://chanomeiei.herokuapp.com/Sweet_test


As a customer

I want to be able to select size of the milk tea

So that I can choose size I want

- User stories size selection test : https://chanomeiei.herokuapp.com/Size_test


As a customer

I want to be able to select topping of the milk tea

So that I can choose topping I like

- User stories topping selection test : https://chanomeiei.herokuapp.com/Topping_test